
@extends('layouts.app')
@section('content')
<div class="container">


<table class="table table-striped">


<thead>
  <th>ID</th>
  <th>Port</th>
  <th>Date_Maree</th>
  <th>HPM</th>
  <th>HBM</th>
  <th>EPM</th>
  <th>EBM</th>
</thead>

@foreach($posts as $value)
  <tr>
    <th scope="row"> {{$value->id}}</th>
    <td>{{$value->port}}</td>
    <td>{{$value->Date_Maree}}</td>
    <td>{{$value->HPM}}</td>
    <td>{{$value->HBM}}</td>
    <td>{{$value->EPM}}</td>
    <td>{{$value->EBM}}</td>
    <td> <a href="{{route('supprimer',['id' => $value->id])}}"> Suppression</a></td>
  </tr>

  @endforeach
</table>
</div>
@endsection
