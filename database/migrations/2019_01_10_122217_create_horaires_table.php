<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horaires', function (Blueprint $table) {
            $table->date('date');
            $table->unsignedInteger('idPort');

            $table->time('heurePM');
            $table->time('heureBM');
            $table->float('eauPM');
            $table->float('eauBM');
            $table->timestamps();
            
            $table->primary(['date', 'idPort']);
            $table->foreign('idPort')->references('id')->on('ports');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horaires');
    }
}
