<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMareesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marees', function (Blueprint $table) {
          $table->increments('id');
          $table->string('port');
          $table->date('Date_Maree');
          $table->time('HPM');
          $table->time('HBM');
          $table->float('EPM');
          $table->float('EBM');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marees');
    }
}
